#!/usr/bin/env bash

supervisor_init_first(){
    if [[ ! -d "${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}" ]];then
        ${exec_prefix} mkdir -p ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d};
    fi
${exec_prefix} cat <<OEF> ${SUPERVISOR_CONFIG_MAIN:-/etc/supervisor/supervisord.conf}
[unix_http_server]
file=/run/supervisord.sock  ; the path to the socket file
[supervisord]
logfile=/var/log/supervisord.log ; main log file; default $CWD/supervisord.log
[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface
[supervisorctl]
serverurl=unix:///run/supervisord.sock ; use a unix:// URL for a unix socket
[include]
files = ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/*.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true
logfile=/dev/null
[inet_http_server]
port=${SUPERVISOR_CONF_INET_PORT:-0.0.0.0:9009}
username=${SUPERVISOR_CONF_INET_USERNAME:-human}
password=${SUPERVISOR_CONF_INET_PASSWORD:-human}
OEF
    export supervisor_config_dir="${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}"
    export supervisor_config_ext="conf"
}


supervisor_update(){
	${exec_prefix} supervisorctl --configuration=${SUPERVISOR_CONFIG_MAIN:-/etc/supervisor/supervisord.conf} reread;
	${exec_prefix} supervisorctl --configuration=${SUPERVISOR_CONFIG_MAIN:-/etc/supervisor/supervisord.conf} update;
}

supervisor_start(){
	echo "exec funct ${FUNCNAME}"
	${exec_prefix}  /usr/bin/supervisord -c ${SUPERVISOR_CONFIG_MAIN:-/etc/supervisor/supervisord.conf}
}



supervisor_run(){
    supervisor_start
}

supervisor_init_consul_template(){
    ${exec_prefix} echo "Exec funct  ${FUNCNAME[0]}"
    ${exec_prefix} cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/${FUNCNAME[0]}.${SUPERVISOR_CONF_EXT:-conf}
[program:${FUNCNAME[0]}]
command=/usr/local/bin/consul-template -config=/opt/consul-template/config/consul-template-${APP_ROLE}.cfg
exitcodes=0,2
stopsignal=SIGTERM
stopwaitsecs=5
stopasgroup=false
killasgroup=false
autostart=true
autorestart=true
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0
startretries=100
priority=1
OEF
}


supervisor_init_dumb(){
    ${exec_prefix} echo "Exec funct echo ${FUNCNAME[0]}"
    ${exec_prefix} cat << OEF > ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/${FUNCNAME[0]}.${SUPERVISOR_CONF_EXT:-conf}

[supervisord]
nodaemon=true
logfile=/dev/null

[program:${FUNCNAME[0]}]
command=tail -f /dev/null
autostart=true
autorestart=true
user=root
startretries=10
priority=10
stopsignal=QUIT
stopwaitsecs=5
stopasgroup=false
killasgroup=false
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0
OEF
}


supervisor_init_nginx(){
    ${exec_prefix} echo "Exec funct  ${FUNCNAME[0]}"
    ${exec_prefix} cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/${FUNCNAME[0]}.${SUPERVISOR_CONF_EXT:-conf}
[program:${FUNCNAME[0]}]
command=nginx -g 'daemon off;'
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
autorestart=false
startretries=0
stopwaitsecs=10
stopsignal=SIGINT
priority=999
OEF
}


supervisor_init_php_fpm(){
    ${exec_prefix} echo "Exec funct  ${FUNCNAME[0]}"
    ${exec_prefix} cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/${FUNCNAME[0]}.${SUPERVISOR_CONF_EXT:-conf}
[program:${FUNCNAME[0]}]
command=/usr/local/sbin/php-fpm -F --pid /var/run/php-fpm.pid
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
autorestart=false
startretries=0
stopwaitsecs=10
stopsignal=SIGINT
OEF
}

supervisor_init_harpoxy(){
    ${exec_prefix} cat << OEF > ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/haproxy.${SUPERVISOR_CONF_EXT:-conf}

[supervisord]
nodaemon=true
logfile=/dev/null


[program:haproxy]
command=/usr/local/sbin/haproxy  -f /etc/haproxy/haproxy.conf  -p /usr/share/haproxy/haproxy.pid
exitcodes=0,2
stopsignal=SIGUSR1
stopwaitsecs=2
stopasgroup=false
killasgroup=false
autostart=true
autorestart=true
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0
startretries=100
OEF
}


supervisor_init_exporter_haproxy(){
    ${exec_prefix} echo "Exec funct echo ${FUNCNAME[0]}"
    ${exec_prefix} cat << OEF > ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/${FUNCNAME[0]}.${SUPERVISOR_CONF_EXT:-conf}

[supervisord]
nodaemon=true
logfile=/dev/null


[program:${FUNCNAME[0]}]
command=/etc/prometheus/exporters/haproxy_exporter --web.listen-address=":19101" --haproxy.scrape-uri=unix:/usr/share/haproxy/hapee-lb.sock
exitcodes=0,2
stopsignal=SIGTERM
stopwaitsecs=5
stopasgroup=false
killasgroup=false
autostart=true
autorestart=true
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0
startretries=100
OEF
}




supervisord_init_consumer-portal(){
if [[ ! -d "${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/" ]];then
    mkdir -p ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/
else
    rm -rfv ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/*
fi

cat <<OEF > ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/consumer-portal.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:consumer-portal]
command=sh -c "yarn start"
directory=${APP_DIR_CURRENT}
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=0
exitcodes=0,2
stopsignal=HUP
stopwaitsecs=10
autostart=true
autorestart=true
redirect_stderr=true
#user=www-data
numprocs=1


OEF
}


supervisor_init_httpd(){
    ${exec_prefix} echo "Exec funct echo ${FUNCNAME[0]}"
    ${exec_prefix} cat << OEF > ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/${FUNCNAME[0]}.${SUPERVISOR_CONF_EXT:-conf}

[supervisord]
nodaemon=true
logfile=/dev/null


[program:${FUNCNAME[0]}]
command=/usr/sbin/httpd -D FOREGROUND -k start
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
autorestart=false
startretries=0
OEF
}

supervisor_init_uwsgi(){
    ${exec_prefix} echo "Exec funct echo ${FUNCNAME[0]}"
    ${exec_prefix} cat << OEF > ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/${FUNCNAME[0]}.${SUPERVISOR_CONF_EXT:-conf}

[supervisord]
nodaemon=true
logfile=/dev/null

[program:${FUNCNAME[0]}]
directory=${APP_DIR_CURRENT}
command=$(which uwsgi) --chdir=${APP_DIR_CURRENT} --module=web.run:application --socket=0.0.0.0:8000 --uid=${DOCKER_IMAGE_RUN_USER:-www-data} --gid=${DOCKER_IMAGE_RUN_USER:-www-data} --vacuum --die-on-term --processes=${UWSGI_PROCESSES_COUNT:-5} --enable-threads --master
process_name=%(program_name)s
autostart=true
autorestart=true
user=${DOCKER_IMAGE_RUN_USER:-www-data}
startretries=10
priority=999
stopsignal=QUIT
stopwaitsecs=5
stopasgroup=false
killasgroup=false
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0
OEF
}


supervisor_init_uwsgi_sms-registration-bot(){
    ${exec_prefix} echo "Exec funct echo ${FUNCNAME[0]}"
    ${exec_prefix} cat << OEF > ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/${FUNCNAME[0]}.${SUPERVISOR_CONF_EXT:-conf}

[supervisord]
nodaemon=true
logfile=/dev/null

[program:${FUNCNAME[0]}]
directory=${APP_DIR_CURRENT}
command=${APP_EXEC_PREARG} $(which uwsgi) --chdir=${APP_DIR_CURRENT} --module=web.run:application --socket=0.0.0.0:8000 --uid=${DOCKER_IMAGE_RUN_USER:-www-data} --gid=${DOCKER_IMAGE_RUN_USER:-www-data} --vacuum --die-on-term --processes=${UWSGI_PROCESSES_COUNT:-5} --enable-threads --master
process_name=%(program_name)s
autostart=true
autorestart=true
user=${DOCKER_IMAGE_RUN_USER:-www-data}
startretries=10
priority=999
stopsignal=QUIT
stopwaitsecs=5
stopasgroup=false
killasgroup=false
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0
OEF
}

supervisor_init_cron(){
    ${exec_prefix} echo "Exec funct echo ${FUNCNAME[0]}"
    ${exec_prefix} cat << OEF > ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/${FUNCNAME[0]}.${SUPERVISOR_CONF_EXT:-conf}

[program:${FUNCNAME[0]}]
nodaemon=true
logfile=/dev/null

[program:${FUNCNAME[0]}]
command= /usr/sbin/cron -f
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
stopwaitsecs=5
user=root
OEF

}


supervisor_init_telegram_bot(){
    ${exec_prefix} echo "Exec funct echo ${FUNCNAME[0]}"
    ${exec_prefix} cat << OEF > ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/${FUNCNAME[0]}.${SUPERVISOR_CONF_EXT:-conf}

[program:${FUNCNAME[0]}]
nodaemon=true
logfile=/dev/null

[program:${FUNCNAME[0]}]
command=python3 ${APP_DIR_CURRENT}/telegram-bot-infra.py
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
stopwaitsecs=5
user=${DOCKER_IMAGE_RUN_USER:-www-data}
OEF

}

supervisor_init_binance-captcha-solver(){
    ${exec_prefix} echo "Exec funct echo ${FUNCNAME[0]}"
    ${exec_prefix} cat << OEF > ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/${FUNCNAME[0]}.${SUPERVISOR_CONF_EXT:-conf}

[supervisord]
nodaemon=true
logfile=/dev/null

[program:${FUNCNAME[0]}]
directory=${APP_DIR_CURRENT}
command=$(which uwsgi) --chdir=${APP_DIR_CURRENT} --module=web.run:application --socket=0.0.0.0:8000 --uid=${DOCKER_IMAGE_RUN_USER:-www-data} --gid=${DOCKER_IMAGE_RUN_USER:-www-data} --vacuum --die-on-term --processes=${UWSGI_PROCESSES_COUNT:-5} --enable-threads --master
process_name=%(program_name)s
autostart=true
autorestart=true
user=${DOCKER_IMAGE_RUN_USER:-www-data}
startretries=10
priority=999
stopsignal=INT
stopwaitsecs=5
stopasgroup=false
killasgroup=false
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0
OEF
}

supervisor_init_geetest-captcha-solver(){
    ${exec_prefix} echo "Exec funct echo ${FUNCNAME[0]}"
    ${exec_prefix} cat << OEF > ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/${FUNCNAME[0]}.${SUPERVISOR_CONF_EXT:-conf}

[supervisord]
nodaemon=true
logfile=/dev/null

[program:${FUNCNAME[0]}]
directory=${APP_DIR_CURRENT}
command=$(which uwsgi) --chdir=${APP_DIR_CURRENT} --module=web.run:application --socket=0.0.0.0:8000 --uid=${DOCKER_IMAGE_RUN_USER:-www-data} --gid=${DOCKER_IMAGE_RUN_USER:-www-data} --vacuum --die-on-term --processes=${UWSGI_PROCESSES_COUNT:-5} --enable-threads --master
process_name=%(program_name)s
autostart=true
autorestart=true
user=${DOCKER_IMAGE_RUN_USER:-www-data}
startretries=10
priority=999
stopsignal=INT
stopwaitsecs=5
stopasgroup=true
killasgroup=true
redirect_stderr=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0
OEF
}

supervisor_init_pm2_binance-captcha-solver(){
    ${exec_prefix} echo "Exec funct echo ${FUNCNAME[0]}"
    ${exec_prefix} cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/${FUNCNAME[0]}.${SUPERVISOR_CONF_EXT:-conf}

[program:${FUNCNAME[0]}]
nodaemon=true
logfile=/dev/null

[program:${FUNCNAME[0]}]
directory=${APP_DIR_CURRENT}
command=/bin/bash -c 'source /home/${DOCKER_IMAGE_RUN_USER:-www-data}/.bash_profile_2 && $(which proxychains4) pm2 start ecosystem.config.js --no-daemon'
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
stopwaitsecs=5
user=${DOCKER_IMAGE_RUN_USER:-www-data}
OEF

}


