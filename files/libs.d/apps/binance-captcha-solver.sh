#!/usr/bin/env bash
export PATH="/home/${DOCKER_IMAGE_RUN_USER:-www-data}/.local/bin:/opt/java/openjdk/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"