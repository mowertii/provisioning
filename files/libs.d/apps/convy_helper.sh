#!/usr/bin/env bash
set -e
app_action=$1

convy_sms_generator_update_start(){
	docker exec -i -u 0 $(docker ps --filter name=convy_dispatcher --filter status=running -q) sh -c 'curl -L https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/apps/convy.sh -o /tmp/libs_convy.sh && source /tmp/libs_convy.sh && convy_sms_generator_update_start'

}

convy_sms_generator_update_finish(){
	docker exec -i -u 0 $(docker ps --filter name=convy_dispatcher --filter status=running -q) sh -c 'curl -L https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/apps/convy.sh -o /tmp/libs_convy.sh && source /tmp/libs_convy.sh && convy_sms_generator_update_finish'

}


eval ${app_action}