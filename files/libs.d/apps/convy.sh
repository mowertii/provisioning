#!/usr/bin/env bash
export dir_app="/var/www/html/${APP_HTTP_HOST:-${APP_HTTP_HOST:-${CONVY_HTTP_HOST}}}"
export dir_shared="${APP_DIR_SHARED:-${dir_app}/shared}"
export dir_current="${APP_DIR_CURRENT:-${dir_app}/current}"


convy_fix_perms(){
	echo "exec funct ${FUNCNAME}"
	cd "${dir_current}" &&  php artisan storage:link
	if [[ -d "${dir_shared}" ]];then
		screen -dmSL "${FUNCNAME}"_find sh -c "find ${dir_app}  \! -user "${DOCKER_IMAGE_RUN_USER:-www-data}" -exec chown -h -v www-data:${DOCKER_IMAGE_RUN_USER:-www-data} {} +; \
		find ${dir_app}  \! -group ${DOCKER_IMAGE_RUN_USER:-www-data} -exec chown -h -v www-data:${DOCKER_IMAGE_RUN_USER:-www-data} {} +;"
	fi
}
convy_fix_docs_generate(){
	echo "exec funct ${FUNCNAME}"
	if [[ -d "${dir_current}" ]];then
		if [[ -f "${dir_current}/storage/app/scribe/openapi.yaml" ]];then
			mv -f "${dir_current}"/storage/app/scribe/openapi.yaml "${dir_current}"/storage/app/scribe/openapi.yaml.old
		fi
		screen -dmSL "${FUNCNAME}" sh -c "cd ${dir_current} && php artisan docs:generate; \
		find ${dir_app}  \! -user "${DOCKER_IMAGE_RUN_USER:-www-data}" -exec chown -h -v www-data:${DOCKER_IMAGE_RUN_USER:-www-data} {} +; \
		find ${dir_app}  \! -group ${DOCKER_IMAGE_RUN_USER:-www-data} -exec chown -h -v www-data:${DOCKER_IMAGE_RUN_USER:-www-data} {} +; \
		echo 'Docs generated'"
	fi
}

convy_clean_cache(){
if [[ -d "${dir_current}/bootstrap/cache" ]];then
	cd "${dir_current}"/bootstrap/cache && rm -rfv config.php services.php packages.php
fi
   cd "${dir_current}" && php artisan cache:clear;php artisan route:cache;php artisan config:cache;php artisan optimize;
   convy_app_perms_fix

}

convy_php_artisan_update_run(){
	echo "exec funct ${FUNCNAME}"
	if [[ -d "${dir_current}" ]];then
		screen -dmSL "${FUNCNAME}" sh -c "cd ${dir_current} && php artisan update:run; echo \"exec funct ${FUNCNAME} finished\""
	fi
}

convy_worker_configs_init(){
	if  grep -q 'OCTANE_SERVER=roadrunner' "${dir_current}/.env" && grep -q  'roadrunner' "${dir_current}/composer.json"; then
		cat <<OEF> "${dir_current}"/.rr.yaml
http:
  address: 127.0.0.1:8000
  pool:
   num_workers: ${OCTANE_WORKERS:-8}
   max_jobs: ${OCTANE_MAX_REQUESTS:-500}
   supervisor:
      exec_ttl: 30s

logs:
  mode: production
  level: ${ROADRUNNER_LOG_LEVEL:-debug}
  output: stdout
  encoding: json		
OEF
		OCTANE_EXTRA_PARAMS="--server=roadrunner --host=127.0.0.1  --port=8000 --workers=${OCTANE_WORKERS:-auto} --task-workers=${OCTANE_TASK_WORKERS:-auto} --max-requests=${OCTANE_MAX_REQUESTS:-500} --rr-config=${dir_current}/.rr.yaml --rpc-port=${OCTANE_RPC_PORT:-16001}"
	else
		OCTANE_EXTRA_PARAMS="--server=swoole --host=127.0.0.1  --port=8000 --workers=${OCTANE_WORKERS:-auto} --task-workers=${OCTANE_TASK_WORKERS:-auto} --max-requests=${OCTANE_MAX_REQUESTS:-500}"
	fi

cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/worker.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true


[program:php-fpm]
command=/usr/local/sbin/php-fpm -F --pid /var/run/php-fpm.pid
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
autorestart=false
startretries=0
stopwaitsecs=10
stopsignal=SIGINT

[program:nginx]
command=nginx -g 'daemon off;'
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
autorestart=false
startretries=0
stopwaitsecs=10
stopsignal=SIGINT
OEF


if [[ ! -f "${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/websockets.${SUPERVISOR_CONF_EXT:-conf}" ]];then
cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/websockets.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:websockets]
command=php ${dir_current}/artisan websockets:serve --port=6001
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
user=www-data
startretries=140
stopwaitsecs=10
OEF
fi

	if [[ -f "${dir_shared}/p0f.fp" ]];then
   cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/p0f.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:p0f]
command=/usr/bin/p0f -s /var/run/p0f.sock -f ${dir_shared}/p0f.fp
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
stopwaitsecs=10
user=root
OEF

fi
	if [[ -f "${dir_shared}/octane" ]];then
      cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/octane.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:octane]
directory=${dir_current}
command=php artisan octane:start ${OCTANE_EXTRA_PARAMS}
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
stopwaitsecs=10
user=www-data
OEF

	if [ ! -f "${dir_current}/storage/logs/swoole_http.log" ];then
		touch "${dir_current}"/storage/logs/swoole_http.log
		chown www-data:"${DOCKER_IMAGE_RUN_USER:-www-data}" "${dir_current}"/storage/logs/swoole_http.log
	else
		chown www-data:"${DOCKER_IMAGE_RUN_USER:-www-data}" "${dir_current}"/storage/logs/swoole_http.log
	fi

else
	echo "File ${dir_shared}/octane not exist. Skip run octane"
	if [[ -f "${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/octane.${SUPERVISOR_CONF_EXT:-conf}" ]];then
		rm "${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/octane.${SUPERVISOR_CONF_EXT:-conf}"
	fi
	
fi

}
convy_dispatcher_configs_init(){

	echo "exec funct ${FUNCNAME}"
	if [[ -f "${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/worker.${SUPERVISOR_CONF_EXT:-conf}" ]];then
		rm ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/worker.${SUPERVISOR_CONF_EXT:-conf}
	fi

cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/nginx.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:nginx]
command=nginx -g 'daemon off;'
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stderr_logfile=/dev/stderr
stdout_logfile_maxbytes=0
stderr_logfile_maxbytes=0
startretries=140
stopwaitsecs=20
OEF

cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/dispatcher.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:dispatcher]
command=php ${dir_current}/artisan horizon
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
user=www-data
startretries=140
stopwaitsecs=20
OEF

cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/websockets.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:websockets]
command=php ${dir_current}/artisan websockets:serve --port=6001
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
user=www-data
startretries=140
stopwaitsecs=10
OEF


cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/cron.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:crond]
command=/usr/sbin/crond -f -l 0 -d 0 
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
stopwaitsecs=10
user=root
OEF


if [[ -f "${dir_shared}/p0f.fp.dispatcher" ]];then

cat <<OEF> ${SUPERVISOR_CONF_DIR:-/etc/supervisor/conf.d}/p0f.${SUPERVISOR_CONF_EXT:-conf}
[supervisord]
nodaemon=true

[program:p0f]
command=/usr/bin/p0f -s /var/run/p0f.sock -f ${dir_shared}/p0f.fp.dispatcher
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
startretries=140
stopwaitsecs=10
user=root
OEF

fi

if [[ ! -d "/etc/periodic/everyminute" ]];then
 mkdir -p /etc/periodic/everyminute
fi

cat <<OEF> /etc/periodic/everyminute/schedule
#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
su -s /bin/bash -c 'cd ${dir_current} && /usr/local/bin/php artisan schedule:run' www-data
OEF

if [[ -f "/etc/periodic/everyminute/schedule" ]];then
	chmod 0755 /etc/periodic/everyminute/schedule
fi

echo "* * * * * run-parts /etc/periodic/everyminute " > /var/spool/cron/crontabs/root && chown -R -v  root /etc/periodic/

}

convy_init(){
	echo "exec funct ${FUNCNAME}"
	if [[ -f ${dir_shared}/.env ]];then
	 ln -sf "${dir_shared}"/.env "${dir_current}"/.env
	fi
	if [[ -f ${dir_shared}/auth.json ]];then
		ln -sf "${dir_shared}"/auth.json "${dir_current}"/auth.json;
	fi
	if [[  ! -L "${dir_current}/storage" ]]; then
	 	mv "${dir_current}"/storage "${dir_current}"/storage.old
	 	ln -sf "${dir_shared}"/storage "${dir_current}"/storage
	fi
	if [[  ! -L "${dir_current}/bootstrap/cache" ]];then
		mv "${dir_current}"/bootstrap/cache "${dir_current}"/bootstrap/cache.old
		ln -sf "${dir_shared}"/bootstrap/cache "${dir_current}"/bootstrap/cache;
	fi

	if [[ "${ENV_WORKER_FUNCTION}" == "dispatcher" ]];then
		cd "${dir_current}";
		convy_clean_cache;
		convy_php_artisan_update_run;
		convy_fix_docs_generate;
		cd "${dir_current}";
		if [[ -n "${APP_VERSION}" ]];then
			grep -q "VUE_APP_VERSION" "${dir_shared}"/.env && echo exists || sed -i "\$aVUE_APP_VERSION=${APP_VERSION}" "${dir_shared}"/.env;
			sed -i  "s/VUE_APP_VERSION=.*$/VUE_APP_VERSION=${APP_VERSION}/g"  "${dir_shared}"/.env;
		fi
		php artisan tracked-processes:start

	fi

	if [[ "${ENV_WORKER_FUNCTION}" == "worker" ]];then
		cd "${dir_current}";
		convy_clean_cache
		convy_fix_perms
		cd "${dir_current}" && php artisan octane:stop || echo "Skip stop octane"
		cd "${dir_current}" && php artisan octane:reload || echo "Skip reload octane"
	fi

}

nginx_config_generate_backend_convy () {

if [[ ! -f "${nginx_main_dir:-/etc/nginx}/nginx_backend_convy_additional.conf" ]];then
    touch ${nginx_main_dir:-/etc/nginx}/nginx_backend_convy_additional.conf
fi

if [[ ! -d "${nginx_dir_backend:-/etc/nginx/backend.d}" ]];then
	mkdir -p ${nginx_dir_backend:-/etc/nginx/backend.d}
fi

if [[ -f "${nginx_main_dir:-/etc/nginx}/nginx_backend_convy_additional.conf" ]];then
    cat <<OEF> ${nginx_main_dir:-/etc/nginx}/nginx_backend_convy_additional.conf
location ~ /\.git {
    deny all;
} 
#set_real_ip_from 0.0.0.0/0;
#real_ip_header X-Forwarded-For;
set \$realip \$remote_addr;
  if (\$http_x_forwarded_for ~ "^(\d+\.\d+\.\d+\.\d+)") {
    set \$realip \$1;
  }

OEF

fi
if [[ -f "${dir_shared}/octane" ]];then
    cat <<OEF>> ${nginx_main_dir:-/etc/nginx}/nginx_backend_convy_additional.conf
location /api/xml {
    proxy_pass   http://127.0.0.1:8000/api/xml;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-Server \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_connect_timeout 120;
    proxy_send_timeout 120;
    proxy_read_timeout 180;
    proxy_pass_request_headers on;
    proxy_set_header Accept-Encoding "" ;
}  

location /api/cdr {
    proxy_pass   http://127.0.0.1:8000/api/cdr;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-Server \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_connect_timeout 120;
    proxy_send_timeout 120;
    proxy_read_timeout 180;
    proxy_pass_request_headers on;
    proxy_set_header Accept-Encoding "" ;
} 
location /api/sms/receive {
    proxy_pass   http://127.0.0.1:8000/api/sms/receive;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-Server \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_connect_timeout 120;
    proxy_send_timeout 120;
    proxy_read_timeout 180;
    proxy_pass_request_headers on;
    proxy_set_header Accept-Encoding "" ;
}
location /api/dlr/report {
    proxy_pass   http://127.0.0.1:8000/api/dlr/report;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-Server \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_connect_timeout 120;
    proxy_send_timeout 120;
    proxy_read_timeout 180;
    proxy_pass_request_headers on;
    proxy_set_header Accept-Encoding "" ;
}  
location /api/conversion/report {
    proxy_pass   http://127.0.0.1:8000/api/conversion/report;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-Server \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_connect_timeout 120;
    proxy_send_timeout 120;
    proxy_read_timeout 180;
    proxy_pass_request_headers on;
    proxy_set_header Accept-Encoding "" ;
}
location /api/conversion/event {
    proxy_pass   http://127.0.0.1:8000/api/conversion/event;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-Server \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_connect_timeout 120;
    proxy_send_timeout 120;
    proxy_read_timeout 180;
    proxy_pass_request_headers on;
    proxy_set_header Accept-Encoding "" ;
}
location /api/bread/ {
    proxy_pass   http://127.0.0.1:8000/api/bread/;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-Server \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_connect_timeout 120;
    proxy_send_timeout 120;
    proxy_read_timeout 180;
    proxy_pass_request_headers on;
    proxy_set_header Accept-Encoding "" ;
}
OEF
fi


for app_http_host in $(echo "${APP_HTTP_HOST:-${CONVY_HTTP_HOST}}" | tr "," "\n")
do
	echo "app_http_host is ${app_http_host}"
	if [[ -n "${app_http_host/[ ]*\n/}" ]]
	then
		echo " Генерируем nginx config ${app_http_host} для nginx_backend_convy"
		if [[ ! -f "${nginx_dir_backend:-/etc/nginx/backend.d}/${app_http_host}.conf" ]]
		then
			touch "${nginx_dir_backend:-/etc/nginx/backend.d}"/"${app_http_host}".conf
		fi
		grep -q "${NGINX_CONVY_HOST_HTTP}" "${nginx_dir_backend:-/etc/nginx/backend.d}"/"${app_http_host}".conf ||  /bin/cat <<OEF>> ${nginx_dir_backend:-/etc/nginx/backend.d}/"${app_http_host}".conf
		###NGINX BACKEND CONFIG BEGIN for ${app_http_host}
		server {
			listen 80;
			server_name 
			${app_http_host}
			www.${app_http_host}
			;
			root ${dir_current}/public;
			index index.php index.html index.htm;     
			include ${nginx_main_dir:-/etc/nginx}/nginx_backend_convy_additional.conf;
			location / {
				try_files \$uri \$uri/ /index.php?\$query_string;
			} 
			lingering_time 60;
			location ~ \.php$ {
				try_files \$uri /index.php =404;
				fastcgi_split_path_info ^(.+\.php)(/.+)$;
				fastcgi_pass ${NGINX_PHP_BACKEND:-127.0.0.1:9000};
				fastcgi_index index.php;
				fastcgi_read_timeout 500;
				fastcgi_send_timeout 500;
				fastcgi_param  SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
				fastcgi_param  PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/node/bin:/bin;
				include fastcgi_params;

			}
		}
		###NGINX BACKEND CONFIG END for ${app_http_host}
OEF

		fi
done

}



convy_app_perms_fix(){
   find ${dir_app}  \! -user "${DOCKER_IMAGE_RUN_USER:-www-data}" -exec chown -h  www-data:"${DOCKER_IMAGE_RUN_USER:-www-data}" {} +;
   find ${dir_app}  \! -group "${DOCKER_IMAGE_RUN_USER:-www-data}" -exec chown -h  www-data:"${DOCKER_IMAGE_RUN_USER:-www-data}" {} +;
}

convy_ccd(){
	if [[ -d ${dir_current} ]];then
		cd ${dir_current}
	else
		echo "dir ${dir_current} not exist"
	fi
}

convy_octane_restart(){
	su -s /bin/bash -c "cd ${dir_current} && /usr/local/bin/php artisan octane:reload && php artisan octane:stop " ${DOCKER_IMAGE_RUN_USER:-www-data};
    su -s /bin/bash -c "supervisorctl restart octane && supervisorctl status octane"
}


convy_fix_perms(){
	echo "exec funct ${FUNCNAME}"
	cd ${dir_current} && php artisan storage:link
	if [[ -d "${APP_DIR_SHARED:-/var/www/html/${APP_HTTP_HOST:-${CONVY_HTTP_HOST}}/shared}" ]];then
	    sh -c "find ${dir_app} \! -user "${DOCKER_IMAGE_RUN_USER:-www-data}" -exec chown -h -v www-data:${DOCKER_IMAGE_RUN_USER:-www-data} {} +;find ${dir_app} \! -group www-data -exec chown -h -v www-data:${DOCKER_IMAGE_RUN_USER:-www-data} {} +;"
	fi
}

convy_tinker(){
	su -s /bin/bash -c "cd ${dir_current} && /usr/local/bin/php artisan tinker" ${DOCKER_IMAGE_RUN_USER:-www-data} 
}

convy_rebuild_worker(){
	sh -c "cd ${dir_current}/frontend ;  yarn build ; cd ${dir_current}/ ; php artisan config:clear; php artisan  cache:clear; php artisan optimize"
	convy_fix_perms
}

convy_rebuild_dispatcher(){
	sh -c "cd ${dir_current}/frontend ;  yarn build ; cd ${dir_current}/ ; php artisan config:clear; php artisan  cache:clear; php artisan optimize; supervisorctl restart all;cd ${dir_current};php artisan config:cache;if [ -f ${dir_current}/storage/app/scribe/openapi.yaml ];then mv -f ${dir_current}/storage/app/scribe/openapi.yaml ${dir_current}/storage/app/scribe/openapi.yaml.old;fi;php artisan docs:generate"
	convy_fix_perms
}


convy_queue_restart(){
	sh -c "cd ${dir_current} ;php artisan queue:restart;supervisorctl restart dispatcher"
	convy_fix_perms
}

convy_sms_generator_update_start () {
   echo "exec ${dir_current} && php artisan sms-generator:update-start";
   cd ${dir_current} && php artisan sms-generator:update-start;
   echo "exec ${dir_current} && php artisan sms-generator:update-start end";
}


convy_sms_generator_update_finish () {
   echo "exec ${dir_current} && php artisan sms-generator:update-finish";
   cd ${dir_current} && php artisan sms-generator:update-finish;
   echo "exec ${dir_current} && php artisan sms-generator:update-finish end";
}
