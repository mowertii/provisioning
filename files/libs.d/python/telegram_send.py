#!/usr/bin/env python3
import requests
from requests.auth import HTTPDigestAuth
def telegram_send(t_bot_token, t_group_id, t_text):
    url = 'https://api.telegram.org/bot' + t_bot_token + '/sendMessage'
    payload = {'chat_id': t_group_id,
               'disable_web_page_preview': 1,
               'disable_notification': 0,
               'text': t_text}
    print('Send to telegram')
    print("url= %(r_url)s payload %(r_payload)s \
        " % {"r_url": url, "r_payload": payload} )    
    r = requests.post(url, data=payload)