#!/usr/bin/env python
# run like so:
# $ python mdprev.py --port=8000 --input=your_markdown_file.md
# then open your browser to http://localhost:8000/
# requires tornado and markdown2 https://github.com/trentm/python-markdown2

import os.path
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.template
from tornado.options import define, options
define("port", default=8000, help="run on the given port", type=int)
define("input", default="README.md", help="read this markdown file")

class MarkdownHandler(tornado.web.RequestHandler):
  def get(self):
    import markdown2
    md = open(options.input).read()
    rendered = tornado.template.Template("""
<!doctype html><html><head><meta charset="utf-8"><title>Marked Down</title>
<link rel="stylesheet" type="text/css"
  href="http://yui.yahooapis.com/3.3.0/build/cssbase/base-min.css">
<style type="text/css">#container{width: 760px;}
  body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:11pt;}
  h2{background-color:black;color:white;padding:5px;margin-top:1em;}
  h3{border:1px black solid;padding: 5px;margin-top: 1em;}
</style></head><body><div id="container"><header></header>
  <div id="main">{{fragment}}</div><footer></footer>
</div></body>""", autoescape=None).generate(fragment=markdown2.markdown(md))
    self.write(rendered)

tornado.options.parse_command_line()
http_server = tornado.httpserver.HTTPServer(
  tornado.web.Application(handlers=[("/",MarkdownHandler)]))
http_server.listen(options.port)
tornado.ioloop.IOLoop.instance().start()