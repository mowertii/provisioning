#!/usr/bin/env python3
 
'''
events.py - subscribe to all events and print them to stdout
'''
from optparse import OptionParser
import sys
import ESL
 

def main(argv):
    parser = OptionParser()
    parser.add_option('-a', '--auth', dest='auth', default='ClueCon',
                      help='ESL password')
    parser.add_option('-s', '--server', dest='server', default='127.0.0.1',
                      help='FreeSWITCH server IP address')
    parser.add_option('-p', '--port', dest='port', default='8021',
                      help='FreeSWITCH server event socket port')
    (options, args) = parser.parse_args()
    con = ESL.ESLconnection(options.server, options.port, options.auth)

    if con.connected():
    	con.events('plain', 'all')
    	while 1:
    		e = con.recvEvent()
    		if e:
    			print(e.serialize())

if __name__ == '__main__':
    main(sys.argv[1:])
