#!/usr/bin/env python3.9
#-*- coding: utf-8 -*-
import os
import json
import time
import pandas
import json
import numpy as np
import sqlalchemy
from elasticsearch import Elasticsearch, RequestsHttpConnection, exceptions
from elasticsearch.serializer import JSONSerializer
from elasticsearch_dsl import Q
from elasticsearch_dsl import Search
import re

SOURCE_FILENAME = os.getenv('SOURCE_FILENAME', '/tmp/upload.log')
PG_HOST = os.getenv('PG_HOST', '10.28.0.1')
PG_PORT = os.getenv('PG_PORT', '15433')
PG_DBNAME = os.getenv('PG_DBNAME', 'infra_logs')
PG_USER = os.getenv('PG_USER', 'infra_logs')
PG_PASSWORD = os.getenv('PG_PASSWORD', 'infra_logs')
PG_TABLE_NAME = os.getenv('PG_TABLE_NAME', 'demo_log')
start_time = time.time()


def linesplit(sock):
  buffer = sock.recv(4096)
  done = False
  while not done:
    more = sock.recv(4096)
    if not more:
      done = True
    else:
      buffer = buffer+more
  if buffer:
    return buffer

def json_extract(obj, key):
    """Recursively fetch values from nested JSON."""
    arr = []

    def extract(obj, arr, key):
        """Recursively search for values of key in JSON tree."""
        if isinstance(obj, dict):
            for k, v in obj.items():
                if isinstance(v, (dict, list)):
                    extract(v, arr, key)
                elif k == key:
                    arr.append(v)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)
        return arr

    values = extract(obj, arr, key)
    return values



with open(SOURCE_FILENAME, encoding='utf-8') as data_file:
    data = data_file.read()
data = data.replace('\x00','')
data = data.replace('}{','},{')
data = data.replace('}"','')
data = data.replace("\\\\\\", "")
data = data.replace("\\", "")
data = data.replace('"{""','{"')
re.sub("\b.*@timestamp.*", 'KKKKKK', data)
formatted_data = re.sub(r'{"@timestamp.*\"message\"\:\"', "", data)
print(formatted_data)
data_j=json.loads(formatted_data)
#json_acceptable_string = data.replace("\"\"", "")
#obj = json.loads(data)
#print(data)
#for p in json.dumps(data):
#    message = json_extract(data, 'message')
#    print(message)
#print(type(json.loads(data)))
#for i in data:
#    print(i)

#f = open("/tmp/demofile2.txt", "a")
#f.write(data)
#f.close()

print(data_j)


#engine = sqlalchemy.create_engine(
#    "postgresql://" + str(PG_USER) + ":" + str(PG_PASSWORD) + "@" + str(PG_HOST) + ":" + str(PG_PORT) + "/" + str(
#        PG_DBNAME))
#print("\nImport into db " + str(PG_DBNAME) + "db url " + 'postgresql://' + str(PG_USER) + str(PG_HOST) + ":" + str(
#    PG_PORT))
#table_name = "tbl_" + str(PG_TABLE_NAME)
#docs.to_sql(table_name, engine, if_exists="append", chunksize=300)
