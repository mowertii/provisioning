common_init(){
    if [[ "${exec_prefix}" == "root" ]];then
        export exec_prefix=""
    else
        export exec_prefix="sudo"
    fi
}

docker_get_container_ip(){
   ## help "docker_get_container_ip haproxy_mysql_haproxy | grep 'docker_container_ip' | awk '{ print $2}'"
   local container_prefix=$1
   export docker_container_name=$(docker ps --filter name=${container_prefix} --filter status=running -q)
   export docker_container_id=$(docker inspect ${docker_container_name} | jq .[].Id)
   export docker_container_ip=$(docker network inspect docker_gwbridge \
    | jq  ".[].Containers."$docker_container_id".IPv4Address" \
    | sed 's~[^\.[:alnum:]/]\+~~g' | cut -d'/' -f1)
   printf "%s$docker_container_ip\n"
}


docker_image_check(){
    if [ ! -d "${HOME}/.docker" ];then
        mkdir -p ${HOME}/.docker
    fi
    if [ ! -f " ${HOME}/.docker/config.json" ];then
        touch  ${HOME}/.docker/config.json
    fi
docker login -u "$DOCKER_REGISTRY_USER" -p "$DOCKER_REGISTRY_PASSWORD" "$CI_REGISTRY"
grep -q ".*experimental.*enabled*" ${HOME}/.docker/config.json \
        && echo "experimental is true" \
        || ( echo "experimental not  enable. Enable" \
        && sed -i '$ d' ${HOME}/.docker/config.json \
        && echo ',' >> ${HOME}/.docker/config.json \
        && echo '"experimental": "enabled"' >> ${HOME}/.docker/config.json \
        && echo '}' >> ${HOME}/.docker/config.json)

#echo "--------docker-config----------"
#cat ${HOME}/.docker/config.json
#echo "--------docker-config----------"

docker manifest inspect "${DOCKER_IMAGE_WORKER_CI}" > /dev/null
}



docker_cleanup(){
    docker rm -v $(docker ps --filter status=exited -q 2>/dev/null) 2>/dev/null
    docker rmi $(docker images --filter dangling=true -q 2>/dev/null) 2>/dev/null
}

docker_status_pg_bitnami() {
	docker exec -ti -u 33 $(docker ps | grep "post.*repmg.*" | awk '{print $NF}' | head -n 1)  sh -c 'repmgr -f /opt/bitnami/repmgr/conf/repmgr.conf cluster show'
}