#!/usr/bin/env bash

hc_convy_dispatcher(){
	curl -f http://127.0.0.1:81/health
}

hc_convy_worker(){
    if ! grep -q "roadrunner" "/var/www/html/${CONVY_HTTP_HOST:-${APP_HTTP_HOST}}/current/composer.json"; then
        echo "healthcheck swoole"
        curl -f http://127.0.0.1:8000/api/octane/healthcheck/swoole;
    elif grep -q "roadrunner" "/var/www/html/${CONVY_HTTP_HOST:-${APP_HTTP_HOST}}/current/composer.json"; then
        echo "healthcheck roadrunner"
        curl -f http://127.0.0.1:8000/api/octane/healthcheck/roadrunner;
    else
        echo "healthcheck simple"
        curl -f http://127.0.0.1:81/health
    fi
}
main(){
	if [[  "${APP_NAME}" == "convy" && "${ENV_WORKER_FUNCTION}" == "dispatcher" ]] || [[ -n "${ENV_CONVY_ADDONS}" && "${ENV_WORKER_FUNCTION}" == "dispatcher" ]];then
		hc_convy_dispatcher
	fi
	if [[  "${APP_NAME}" == "convy" && "${ENV_WORKER_FUNCTION}" == "worker" ]] || [[ -n "${ENV_CONVY_ADDONS}" && "${ENV_WORKER_FUNCTION}" == "worker" ]];then
		hc_convy_worker
	fi
}
main