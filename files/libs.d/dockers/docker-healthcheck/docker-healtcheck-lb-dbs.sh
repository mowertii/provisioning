#!/usr/bin/env bash

hc_nginx_lb_mysql(){
	curl -f http://127.0.0.1:81/health
}

main(){
	if [[  "${APP_NAME}" == "nginx_lb_mysql" ]] || [[  "${APP_ROLE}" == "nginx_lb_mysql" ]] ;then
		hc_nginx_lb_mysql
	fi
}
main