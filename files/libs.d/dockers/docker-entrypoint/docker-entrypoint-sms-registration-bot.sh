#!/usr/bin/env bash
app_prepare_sms-registration-bot(){

    ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env
    if grep -q "^PROXYCHAINS=1" "${APP_DIR_CURRENT}"/.env;then
        export APP_EXEC_PREARG="/usr/bin/proxychains4 -q"
    else
        export APP_EXEC_PREARG=
    fi
    echo "Crons add for user ${DOCKER_IMAGE_RUN_USER:-www-data}"
    if [[ -f "/var/spool/cron/crontabs/${DOCKER_IMAGE_RUN_USER:-www-data}" ]]; then
        rm /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
    fi
    cat <<OEF> /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
##Crons
OEF

    cat <<OEF> /tmp/crontabs
*/1 * * * * cd ${APP_DIR_CURRENT} && ${APP_EXEC_PREARG} python3.7 utils/proxy_refresh.py >> ${APP_DIR_SHARED}/storage/logs/crons/proxy_refresh.log 2>&1
*/15 * * * * cd ${APP_DIR_CURRENT} && python3.7 utils/captcha_refresh.py  >> ${APP_DIR_SHARED}/storage/logs/crons/captcha_refresh.log 2>&1
0 0 * * * cd ${APP_DIR_CURRENT} && python3.7 health_check.py >> ${APP_DIR_SHARED}/storage/logs/crons/health_check.log 2>&1
0 0 * * * cd ${APP_DIR_CURRENT} && python3.7 utils/clean_logs.py >> ${APP_DIR_SHARED}/storage/logs/crons/clean_logs.log 2>&1
*/5 * * * * cd ${APP_DIR_CURRENT} && python3.7 utils/information.py >> ${APP_DIR_SHARED}/storage/logs/crons/information.log 2>&1
*/1 * * * * cd ${APP_DIR_CURRENT} && echo "\$(date)" >> ${APP_DIR_SHARED}/storage/logs/crons/echo.log 2>&1
OEF

    crontab -l -u ${DOCKER_IMAGE_RUN_USER:-www-data} | cat - /tmp/crontabs | crontab -u ${DOCKER_IMAGE_RUN_USER:-www-data} -
    echo "Cronlist"
    cat /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
    echo "Fix rights"
    chown -v "${DOCKER_IMAGE_RUN_USER:-www-data}":crontab /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"

    echo "Create file ${APP_DIR_CURRENT}/sms-sender.ini"
    cat <<OEF> "${APP_DIR_CURRENT}"/sms-sender.ini
[uwsgi]
module = web.api:app
master = true
processes = 5
chdir = ${APP_DIR_CURRENT}
socket = 0.0.0.0:8000
uid =${DOCKER_IMAGE_RUN_USER:-www-data}
gid = ${DOCKER_IMAGE_RUN_USER:-www-data}
vacuum = true
die-on-term = true
plugin=python3
OEF



}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/nginx.sh
    if [[ "${APP_NAME}" == "sms-registration-bot" ]];then
        app_prepare_sms-registration-bot
        nginx_prepare
        nginx_init_redis_proxy
        supervisor_init_first
        supervisor_init_dumb
        supervisor_init_uwsgi_sms-registration-bot
        supervisor_init_cron
        supervisor_init_nginx

        fix_perms || (echo "skip perms fix")
        supervisor_run
    fi
}

main






