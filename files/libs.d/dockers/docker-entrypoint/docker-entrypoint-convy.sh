#!/usr/bin/env bash
##libs common
source /opt/public_libs/files/libs.d/apps/main.sh
source /opt/public_libs/files/libs.d/apps/docker.sh
source /opt/public_libs/files/libs.d/apps/consul_agent.sh
source /opt/public_libs/files/libs.d/apps/consul_template.sh
source /opt/public_libs/files/libs.d/apps/php.sh
source /opt/public_libs/files/libs.d/apps/nginx.sh
source /opt/public_libs/files/libs.d/apps/supervisor.sh
##libs app
source /opt/public_libs/files/libs.d/apps/convy.sh

main(){

	if [[ "${ENV_WORKER_FUNCTION}" == "worker" ]];then
		supervisor_init_first
		consul_agent_start
		nginx_config_generate_backend_convy
		convy_init
		convy_worker_configs_init
		convy_app_perms_fix
		supervisor_start
	fi

	if [[ "${ENV_WORKER_FUNCTION}" == "dispatcher" ]];then
		supervisor_init_first
		consul_agent_start
		nginx_config_generate_backend_convy
		convy_init
		convy_dispatcher_configs_init
		convy_app_perms_fix
		supervisor_start
	fi

}

main


