#!/usr/bin/env bash

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    if [[ "${APP_NAME}" == "telegram-bot-infra" ]];then
        supervisor_init_first
        supervisor_init_dumb
        supervisor_init_telegram_bot
        fix_perms || (echo "skip perms fix")
        supervisor_run
    fi
}

main





