#!/usr/bin/env bash
app_prepare_binance-captcha-solver (){
    ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env

    cat <<OEF> /etc/proxychains4.conf
    strict_chain
    #proxy_dns
    remote_dns_subnet 224
    tcp_read_time_out 15000
    tcp_connect_time_out 8000
    localnet 127.0.0.0/255.0.0.0
    localnet 10.0.0.0/255.0.0.0
    localnet 172.16.0.0/255.255.0.0
    localnet 192.168.0.0/255.255.0.0
    [ProxyList]
    http ${HTTP_PROXY_HOST:-192.168.100.105} ${HTTP_PROXY_PORT:-3128}
OEF


    echo "Crons add for user ${DOCKER_IMAGE_RUN_USER:-www-data}"
    if [[ -f "/var/spool/cron/crontabs/${DOCKER_IMAGE_RUN_USER:-www-data}" ]]; then
        rm /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
    fi
    if [[ ! -d "${APP_DIR_SHARED}/storage/logs/crons" ]];then
        mkdir -p ${APP_DIR_SHARED}/storage/logs/crons
    fi
    cat <<OEF> /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
##Crons
OEF

    cat <<OEF> /tmp/crontabs
0 0 * * * cd ${APP_DIR_CURRENT} && python3.7 utils/clean_logs.p >> ${APP_DIR_SHARED}/storage/logs/crons/clean_logs.log 2>&1
OEF
    crontab -l -u ${DOCKER_IMAGE_RUN_USER:-www-data} | cat - /tmp/crontabs | crontab -u ${DOCKER_IMAGE_RUN_USER:-www-data} -
    echo "Cronlist"
    cat /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"
    echo "Fix rights"
    chown -v "${DOCKER_IMAGE_RUN_USER:-www-data}":crontab /var/spool/cron/crontabs/"${DOCKER_IMAGE_RUN_USER:-www-data}"

}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    if [[ "${APP_NAME}" == "binance-captcha-solver" ]];then
        app_prepare_binance-captcha-solver
        supervisor_init_first
        supervisor_init_dumb
        supervisor_init_binance-captcha-solver
        supervisor_init_pm2_binance-captcha-solver
        supervisor_init_cron
        fix_perms || (echo "skip perms fix")
        supervisor_run
    fi
}

main






