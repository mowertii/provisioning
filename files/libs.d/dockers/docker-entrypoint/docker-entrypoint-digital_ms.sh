#!/usr/bin/env bash
app_prepare_digital_ms(){
    ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env
    if [[  ! -L "${APP_DIR_CURRENT}/storage" ]];then
        mv ${APP_DIR_CURRENT}/storage ${APP_DIR_CURRENT}/storage.old
        ln -sf ${APP_DIR_SHARED}/storage ${APP_DIR_CURRENT}/storage
    fi
    if [[ ! -d "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
        mkdir -p ${APP_DIR_CURRENT}/bootstrap/cache
    fi
    if [[  ! -L "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
        if [[ -d "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
            mv ${APP_DIR_CURRENT}/bootstrap/cache ${APP_DIR_CURRENT}/bootstrap/cache.old
        fi
        ln -sf ${APP_DIR_SHARED}/bootstrap/cache ${APP_DIR_CURRENT}/bootstrap/cache
    fi

}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/nginx.sh
    if [[ "${APP_NAME}" == "digital_ms" ]];then
        app_prepare_digital_ms
        supervisor_init_first
        supervisor_init_nginx
        supervisor_init_php_fpm
        nginx_config_gen_http_backend_digital_ms
        fix_perms || (echo "skip perms fix")
        supervisor_run
    fi
}

main






