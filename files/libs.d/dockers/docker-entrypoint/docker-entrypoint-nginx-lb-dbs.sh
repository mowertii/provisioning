#!/usr/bin/env bash
export exec_prefix=root
source /opt/libs.d/dockers/common.sh
source /opt/libs.d/dockers/nginx/common.sh
source /opt/libs.d/dockers/consul/consul_agent.sh
source /opt/libs.d/dockers/consul/consul_template.sh
source /opt/libs.d/dockers/supervisor/lib.sh


main(){
	common_init
	nginx_prepare
	nginx_acl_gen_mysql
	consul_template_prepare
	consul_template_gen_nginx_mysql
	consul_agent_start
	supervisor_init_first
	supervisor_init_consul_template
	supervisor_init_nginx
	supervisor_run
}

main


