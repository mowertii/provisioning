#!/usr/bin/env bash
app_prepare_geetest-captcha-solver (){
    ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env

    cat <<OEF> /etc/proxychains4.conf
    strict_chain
    #proxy_dns
    remote_dns_subnet 224
    tcp_read_time_out 15000
    tcp_connect_time_out 8000
    localnet 127.0.0.0/255.0.0.0
    localnet 10.0.0.0/255.0.0.0
    localnet 172.16.0.0/255.255.0.0
    localnet 192.168.0.0/255.255.0.0
    [ProxyList]
    http ${HTTP_PROXY_HOST:-10.50.0.119} ${HTTP_PROXY_PORT:-3128}
OEF

}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    if [[ "${APP_NAME}" == "geetest-captcha-solver" ]];then
        app_prepare_geetest-captcha-solver
        supervisor_init_first
        supervisor_init_dumb
        supervisor_init_geetest-captcha-solver
        app_fix_perms || (echo "skip app_fix_perms")
        supervisor_run
    fi
}

main






