#!/usr/bin/env bash
if [[ ! -d "${SQUID_LOG_DIRECTORY:-/var/log/squid}" ]];then
	mkdir -pv "${SQUID_LOG_DIRECTORY:-/var/log/squid}" -m 777
fi

if [[ -n "${SQUID_INIT_CONFIG}" ]];then
	cat <<OEF>  /etc/squid/squid.conf
#####
http_access allow all
http_port 3128
cache deny all
cache_dir null /tmp
coredump_dir ${SQUID_LOG_DIRECTORY:-/var/log/squid}/

#/usr/sbin/squid -k rotate
logfile_rotate 10

cache_log ${SQUID_LOG_DIRECTORY:-/var/log/squid}/cache.log
access_log ${SQUID_LOG_DIRECTORY:-/var/log/squid}/access.log
cache_store_log ${SQUID_LOG_DIRECTORY:-/var/log/squid}/cache_store.log



#####acl all src 0.0.0.0/0
#acl CONNECT method CONNECT
#dns_v4_first on
OEF
fi


/usr/sbin/squid -f /etc/squid/squid.conf --foreground -d 1


