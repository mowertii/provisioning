#!/usr/bin/env bash
app_prepare_sms_cashback(){
    ln -sf ${APP_DIR_SHARED}/.env ${APP_DIR_CURRENT}/.env
    ln -sf ${APP_DIR_SHARED}/.env.local ${APP_DIR_CURRENT}/.env.local
    if [[  ! -L "${APP_DIR_CURRENT}/storage" ]];then
        if [[ -d "${APP_DIR_CURRENT}/storage" ]];then
            mv ${APP_DIR_CURRENT}/storage ${APP_DIR_CURRENT}/storage.old
        fi
        ln -sf ${APP_DIR_SHARED}/storage ${APP_DIR_CURRENT}/storage
    fi
    if [[ ! -d "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
        mkdir -p ${APP_DIR_CURRENT}/bootstrap/cache
    fi
    if [[  ! -L "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
        if [[ -d "${APP_DIR_CURRENT}/bootstrap/cache" ]];then
            mv ${APP_DIR_CURRENT}/bootstrap/cache ${APP_DIR_CURRENT}/bootstrap/cache.old
        fi
        ln -sf ${APP_DIR_SHARED}/bootstrap/cache ${APP_DIR_CURRENT}/bootstrap/cache
    fi

}

main(){
    source /opt/public_libs/files/libs.d/apps/main.sh
    source /opt/public_libs/files/libs.d/apps/supervisor.sh
    source /opt/public_libs/files/libs.d/apps/nginx.sh
    source /opt/public_libs/files/libs.d/apps/apache2.sh
    if [[ "${APP_NAME}" == "sms_cashback" ]];then
        app_prepare_sms_cashback
        supervisor_init_first
        supervisor_init_httpd
        apache2_configs_init_sms_cashback
        fix_perms || (echo "skip perms fix")
        supervisor_run
    fi
}

main






