#!/usr/bin/env bash
nginx_run(){
	nginx -g 'daemon off;'
}

nginx_prepare(){
	if [[ ! -d "/etc/nginx/stream.d" ]];then
		mkdir -p /etc/nginx/stream.d
	fi
}


nginx_autoreload_inotifywait(){
	# inspired/copied from https://github.com/kubernetes/kubernetes/blob/master/examples/https-nginx/auto-reload-nginx.sh

while true
do
        inotifywait -e create -e modify -e delete -e move -r --exclude "\\.certbot\\.lock|\\.well-known" "/etc/nginx"
        echo "Changes noticed in /spcgeonode-certificates"

        echo "Waiting 5s for additionnal changes"
        sleep 5


        # Test nginx configuration
        nginx -t
        # If it passes, we reload
        if [ $? -eq 0 ]
        then
                echo "Configuration valid, we reload..."
                nginx -s reload
        else
                echo "Configuration not valid, we do not reload."
        fi
done
}

nginx_reload(){
	export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games"
	local config_test_nginx=$(/usr/sbin/nginx  -t > /dev/null ; echo "return_code=$?" | awk -F= '{print $NF}')
	if [[ "${config_test_nginx}" == "0" ]];then
		/usr/sbin/nginx -s reload
	fi
}


nginx_acl_gen_mysql(){
    if [[ ! -d "${NGINX_CONFIG_DIsR_ROOT:-/etc/nginx}/acl.d" ]];then
    	mkdir -p "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d"
    fi
    if [ ! -f "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_mysql.conf" ];then 
        touch "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_mysql.conf"
    else
        : > "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_mysql.conf"
    fi

    for IP in $(echo "${NGINX_MYSQL_ACL_LIST_NETWORK:-0.0.0.0/0}" | tr -s " " "\012")
    do
        echo "allow ${IP};" >> "${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_mysql.conf"
done
    echo "deny all;" >> ${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_mysql.conf

    cat ${NGINX_CONFIG_DIR_ROOT:-/etc/nginx}/acl.d/acl_mysql.conf

}


nginx_config_dir_create(){

   if [[ -d "${acme_dir:-/var/www/letsencrypt}" ]]
   then
        mkdir -p "${acme_dir:-/var/www/letsencrypt}"
        if [[ ! -d "/var/tmp/nginx/fastcgi" ]];then
            mkdir -p /var/tmp/nginx/fastcgi
        fi
        chown --recursive www-data ${acme_dir:-/var/www/letsencrypt}
        chown --recursive www-data:www-data /var/tmp/nginx
   fi
}