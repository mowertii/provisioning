#!/usr/bin/env bash
source_create(){
    export check_files="/tmp/${APP_NAME}-${APP_HTTP_HOST}-shared-vars.sh /tmp/variables.sh"
    for check_file in ${check_files}
    do
    if [[ -f "${check_file}" ]];then 
        rm -v ${check_file}
    fi
done    
    echo "export APP_VERSION=${APP_VERSION}" > /tmp/variables.sh
    curl --header "PRIVATE-TOKEN: ${GITLAB_ACCESS_TOKEN}" "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/variables" | jq -r 'keys[] as $k | "export \($k)=\(.[$k])\""'  | awk -F, '{ print $2$3}' | awk -F':"' '{ print $2$3}' | sed -e 's|""value|=|' | sed -e 's/^/export /' >> /tmp/variables.sh
    export ci_data=$(PGPASSWORD=${POSTGRES_DB_PASSWORD_CI} psql \
        --host=${POSTGRES_HOST_CI} \
        --port=${POSTGRES_PORT_CI} \
        --username=${POSTGRES_DBUSER_CI} \
        --dbname=${POSTGRES_DBNAME_CI} \
        --tuples-only \
        --field-separator ' ' \
        --pset footer=off \
        --no-align \
        --quiet \
        --set ON_ERROR_STOP=on \
        <<EOF
        select data
        from tbl_env
        WHERE data->>'APP_NAME' in ('${APP_NAME}') and data->>'APP_HTTP_HOST' in ('${APP_HTTP_HOST}')
        ORDER BY time DESC limit 1
EOF
        )
        if [[ -z "$ci_data" ]];then
            echo "Error. ci_data empty. exit"
            exit 1
        fi 
        echo $ci_data  | jq -r 'keys[] as $k | "export \($k)=\(.[$k])"' | sort -t\$ -k 2 >> /tmp/variables.sh
        if [[ -n "${APP_BASE_IMAGE}" ]];then
            echo "Replace DOCKER_IMAGE_BASE with APP_BASE_IMAGE for backward compatibility"
            echo "export APP_BASE_IMAGE=${APP_BASE_IMAGE}" >> /tmp/variables.sh
            sed -i "s/^.*DOCKER_IMAGE_BASE=.*$/export DOCKER_IMAGE_BASE=\${APP_BASE_IMAGE}/" /tmp/variables.sh
        fi
        cp /tmp/variables.sh ${APP_NAME}-${APP_VERSION}-${APP_HTTP_HOST}-shared-vars.sh
        cp /tmp/variables.sh /tmp/"${APP_NAME}"-"${APP_HTTP_HOST}"-shared-vars.sh
        cp /tmp/"${APP_NAME}"-"${APP_HTTP_HOST}"-shared-vars.sh "${DIR_GIT_ROOT}"/"${APP_NAME}"-"${APP_VERSION}"-"${APP_HTTP_HOST}"-shared-vars.sh
        echo "============ ${DIR_GIT_ROOT}/${APP_NAME}-${APP_VERSION}-${APP_HTTP_HOST}-shared-vars.sh ============ begin"
        cat "${DIR_GIT_ROOT}"/"${APP_NAME}"-"${APP_VERSION}"-"${APP_HTTP_HOST}"-shared-vars.sh
        echo "============ ${DIR_GIT_ROOT}/${APP_NAME}-${APP_VERSION}-${APP_HTTP_HOST}-shared-vars.sh ============ end"
        if [[ "${?}" == 0 ]];then
            echo "${FUNCNAME} success"
        else
            echo "${FUNCNAME} error"
        fi 
}

source_read(){
    echo source_read "${DIR_GIT_ROOT}"/"${APP_NAME}"-"${APP_VERSION}"-"${APP_HTTP_HOST}"-shared-vars.sh
    source "${DIR_GIT_ROOT}"/"${APP_NAME}"-"${APP_VERSION}"-"${APP_HTTP_HOST}"-shared-vars.sh || (echo "Error during read file ${DIR_GIT_ROOT}/${APP_NAME}-${APP_VERSION}-${APP_HTTP_HOST}-shared-vars.sh" && exit 1)
    if [[ "${?}" == 0 ]];then
        echo "${FUNCNAME} success"
    else
        echo "${FUNCNAME} error"
    fi
    source "${BUILD_COMMON_LIBS_PATH}"/files/libs.d/shell/telegram.sh
    export ssh_options=""
    export app_stack_source_dir=${APP_STACK_SOURCE_FILE%/*}
    export app_stack_target_dir=${APP_STACK_TARGET_FILE%/*}
    export date_update=$(date -u)
}


lib_import_public(){
    if [[ -z "${BUILD_COMMON_LIBS_VCS_URL}" ]];then
        export BUILD_COMMON_LIBS_PATH="/mnt/hdd/git/gitlab/cdn72/provisioning"
        printf "BUILD_COMMON_LIBS_PATH=$BUILD_COMMON_LIBS_PATH\n"
    else
        echo "Exec git clone --depth 1 --branch ${BUILD_COMMON_LIBS_VCS_BRANCH:-master} ${BUILD_COMMON_LIBS_VCS_URL:-https://gitlab.com/cdn72/provisioning.git} /tmp/public_libs"
        git clone --depth 1 --branch ${BUILD_COMMON_LIBS_VCS_BRANCH:-master} ${BUILD_COMMON_LIBS_VCS_URL:-https://gitlab.com/cdn72/provisioning.git} /tmp/public_libs
        ls -la /tmp/public_libs/*
        if [[ "${?}" == 0 ]];then
            export BUILD_COMMON_LIBS_PATH="/tmp/public_libs"
            printf "BUILD_COMMON_LIBS_PATH=$BUILD_COMMON_LIBS_PATH\n"
        else
            echo "Error during clone  ${BUILD_COMMON_LIBS_VCS_URL:-https://gitlab.com/cdn72/provisioning.git}"
            exit 1
        fi
    fi
    source "${BUILD_COMMON_LIBS_PATH}"/files/libs.d/dockers/common.sh
    source "${BUILD_COMMON_LIBS_PATH}"/files/libs.d/shell/telegram.sh
}


private_key_import(){

    if [[ -n $DEPLOY_PRIVATE_KEY ]] ;then
        if [[ ! -d "${DEPLOY_KEY_DIR}" ]];then mkdir -p "${DEPLOY_KEY_DIR}";fi
        echo "$DEPLOY_PRIVATE_KEY" | tr -d '\r'  > "${DEPLOY_KEY_DIR}"/id_rsa
    else
        if [[ ! -d "${DEPLOY_KEY_DIR}" ]];then mkdir -p "${DEPLOY_KEY_DIR}";fi
        cp "${HOME}"/.ssh/id_rsa ${DEPLOY_KEY_DIR}/ || (echo "File ont exist ${HOME}/.ssh/id_rsa" && exit 1)
    fi
}

handler_error(){
    local func_name=$1
    if [[ "$?" == "0" ]];then
        echo "ok ${func_name}"
    else
        echo "error ${func_name}"
        exit 1
    fi  
}


handler_finish(){
    rm -v /tmp/*"${APP_NAME}"*sh
    if [[ -f "${DEPLOY_KEY_DIR}/id_rsa" ]];then
        rm "${DEPLOY_KEY_DIR}"/id_rsa;
    fi
    if [[ -z "${CI_ACTION}" ]] && [[ -f "${DIR_GIT_ROOT}/${APP_NAME}-${APP_VERSION}-${APP_HTTP_HOST}-shared-vars.sh" ]];then
        echo "Delete file ${DIR_GIT_ROOT}/${APP_NAME}-${APP_VERSION}-${APP_HTTP_HOST}-shared-vars.sh"
        rm "${DIR_GIT_ROOT}/${APP_NAME}-${APP_VERSION}-${APP_HTTP_HOST}-shared-vars.sh"
    fi

    
}
get_ci_data_pg_py(){
    python3 "${BUILD_COMMON_LIBS_PATH}"/files/libs.d/python/postgres.py
}

ci_prepare_common(){
    export DIR_GIT_ROOT=$(pwd | awk -F\files '{ print $1}'  | grep "${CI_PROJECT_NAME:-infra-f}" )
    export DOCKER_DIR_ROOT=${DIR_GIT_ROOT}/files/dockers/app/${APP_NAME}/build/build/${APP_NAME}
    export DEPLOY_KEY_DIR=${DOCKER_DIR_ROOT}/files/deploy-keys
}