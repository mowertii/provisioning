telegram_send_message_post(){
	local TEXT=$1
	local USERID=$2
	local KEY=$3
	local TIMEOUT="10"
	local URL="https://api.telegram.org/bot$KEY/sendMessage"
	local DATE_EXEC="$(date "+%d %b %Y %H:%M")"
	local TMPFILE='/tmp/ipinfo-$DATE_EXEC.txt'
	curl  -s --max-time $TIMEOUT -d "chat_id=$USERID&disable_web_page_preview=1&text=$TEXT" $URL > /dev/null
}

telegram_send_message_get(){
	local T_TEXT=$1
	local WEBHOOK_URL=$2
	curl  -X GET -s --max-time ${TIMEOUT:-2} "$WEBHOOK_URL&disable_web_page_preview=1&text=${T_TEXT}" > /dev/null
}

install_dbeaver(){
	cd /tmp
	sudo apt install "./$(
		curl -O "$(
			curl -sL -I -w '%{url_effective}' \
			https://dbeaver.io/files/dbeaver-ce_latest_amd64.deb \
			| tail -1 )" -w '%{filename_effective}'
		)"
	rm -v  dbeaver*.deb
}


system_upgrade(){
	sudo apt-get update ; sudo apt-get dist-upgrade -y && sudo apt-get upgrade -y
	cd /tmp
	git clone https://kernel.googlesource.com/pub/scm/linux/kernel/git/firmware/linux-firmware.git
	sudo curl -L https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/plain/rtl_nic/rtl8125a-3.fw  -o /lib/firmware/rtl_nic/rtl8125a-3.fw
	sudo curl -L https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/plain/rtl_nic/rtl8125b-2.fw  -o /lib/firmware/rtl_nic/rtl8125b-2.fw
	sudo curl -L https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/plain/rtl_nic/rtl8168fp-3.fw -o /lib/firmware/rtl_nic/rtl8168fp-3.fw
	sudo rsync -a linux-firmware/amdgpu/* /lib/firmware/amdgpu/
	sudo update-initramfs -k all -u -v
	install_dbeaver
}

kill_dbeaver() {
	kill -9 $(ps aux | grep dbeav | awk '{ print $2}')
}


kill_vlc() {
	kill -9 $(ps aux | grep '/usr/bin/vlc' | awk '{ print $2}')
}


ssh_agent_start(){
	ssh-add -D ; eval `ssh-agent` ; ssh-add  ~/.ssh/id_rsa ; ssh-add -l
}



youtube_video_get(){
	if [[ -z "$@" ]];then
		echo "Error. Arg1 is empty"
	else
		cd ${youtube_download_dir} && python3 /usr/local/bin/youtube-dl --cookies ${youtube_com_cookies} -f best "$@"
		#youtube-dl -f bestvideo+bestaudio --merge-output-format mkv --all-subs --cookies /git/linux2be.com/itc-life/files/pcs/asus_tuf_gaming_a17/youtube.com_cookies.txt [YouTube URL]
	fi
}

