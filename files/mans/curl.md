# Curl quick start


## Install

**Ubuntu**

```sh
sudo apt install -y curl

```

## Usage

Download file with curl to target directory

```sh
export dir_libs_shell="/opt/scripts/shell"
if [[ ! -d "${dir_libs_shell}" ]];then sudo mkdir -p ${dir_libs_shell};fi
sudo chown -R $(whoami):$(whoami) ${dir_libs_shell}
curl -L https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/shell/common.sh -o ${dir_libs_shell}/common.sh

```