###
ngx.re.gsub(ngx.ctx.buffered, "([\\n\\r<<>>{}])", "");

###


header_filter_by_lua '
ngx.header.content_length = nil
';


body_filter_by_lua '
-- ngx.arg[1] = string.gsub(ngx.arg[1], "hello world", "HIT!")
-- do return end

local chunk, eof = ngx.arg[1], ngx.arg[2]
local buffered = ngx.ctx.buffered

if not buffered then
  buffered = {} -- XXX we can use table.new here
  ngx.ctx.buffered = buffered
end

if chunk ~= "" then
  buffered[#buffered + 1] = chunk
  ngx.arg[1] = nil
end

if eof then
  local whole = table.concat(buffered)
  ngx.ctx.buffered = nil
  whole = string.gsub(whole, "hello world", "HIT!")
  ngx.arg[1] = whole
end
';


underscores_in_headers on;
lua_need_request_body on;
set $req_body "";
set $req_headers "";
set $resp_body_log "";
set $resp_body_log_2 "";
set $resp_headers "";

rewrite_by_lua_block {
    local req_headers = "Headers: ";
    ngx.var.req_body = ngx.req.get_body_data();
    local h, err = ngx.req.get_headers()
    for k, v in pairs(h) do
       req_headers = req_headers .. k .. ": " .. v .. "\n";
    end
    ngx.var.req_headers = req_headers;
    local rh = ngx.resp.get_headers()
    for k, v in pairs(rh) do
       ngx.var.resp_headers = ngx.var.resp_headers .. k.."="..v.." "
    end
}

body_filter_by_lua '
   local resp_body_log = string.sub(ngx.arg[1], 1, -1)
   ngx.ctx.buffered = (ngx.ctx.buffered or "") .. resp_body_log
   if ngx.arg[2] then
      ngx.var.resp_body_log = ngx.ctx.buffered;
   end
   local regex = "\\\\d+"
   local m = ngx.re.match("hello, 1234", regex)
   if m then 
     ngx.var.resp_body_log_2 = resp_body_log
   else 
     ngx.var.resp_body_log_2 = "truncated"
   end


';

    #log_by_lua_block {
    #    resp["body"] = ngx.ctx.response_body
    #    ngx.log(ngx.CRIT, json.encode(data))
    #}
#ngx.re.gsub(resp_body_log_f "test", "example")
##   --local f_resp_body_log = ngx.var.resp_body_log:gsub("\\([ntr\"])", ""))



                #   ngx.req.read_body()
                #   local body = ngx.req.get_body_data()
                #   if body then
                #      body = ngx.re.gsub(body, "SOAP-ENV", "soap")
                #   end
                #   ngx.req.set_body_data(body) 
                #"^[\\x00-\\x09]+"
                ##--ngx.var.resp_body_log_2 = resp_body_log .. "212";
                ##--if ngx.var.resp_body_log ~= "333" then
                ##--    ngx.var.resp_body_log_2 = "truncated"
                ##--end
####
   if ngx.var.remote_addr then
     ngx.log(ngx.ERR, "REMOTE_ADDR", ngx.var.remote_addr)
     ngx.var.resp_body_log_2 = ngx.var.resp_body_log ;
   end
   local data, eof = ngx.arg[1], ngx.arg[2]

   ngx.log(ngx.ERR, "NGINX_VAR_DATA", ngx.var.data)
   ngx.log(ngx.ERR, "NGINX_VAR_OEF", ngx.var.eof)
####




####
   local m, err = ngx.re.match(ngx.var.resp_body_log, "fruit2")
   if m then
     ngx.log(ngx.ERR, "found match: ", m[0])
     ngx.var.resp_body_log_2 = "truncated";
   else
     ngx.log(ngx.ERR, "found not match: ", "regexp", ngx.var.resp_body_log)
     ngx.var.resp_body_log_2 = "ok";
   end

####

   --local m, err = ngx.re.match(ngx.var.resp_body_log, ".*ffdfdf.*", "io")
   --if m then
     --ngx.log(ngx.ERR, "found match: ", m[0])
     --ngx.var.resp_body_log_2 = "truncated";
   --else
     --ngx.log(ngx.ERR, "found not match: ", ngx.var.resp_body_log)
     --ngx.var.resp_body_log_2 = "ok";
   --end
###
   local regex = ".*fruit2.*"
   local m = ngx.re.match(ngx.var.resp_body_log, regex)

   if m then
     ngx.log(ngx.ERR, "Enable truncate ")
     ngx.var.resp_body_log_2 = "truncated";
   else
     ngx.log(ngx.ERR, "Skip truncate: ")
     ngx.var.resp_body_log_2 = ngx.var.resp_body_log;
   end
   
###

   local m = ngx.re.match(ngx.var.resp_body_log, "ffdfdf")
   if m then
     ngx.say("matched: ", m[0])
   else
     ngx.say("no match.")
   end