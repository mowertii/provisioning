## Description

sudo (/suːduː/[4] or /ˈsuːdoʊ/[4][5]) is a program for Unix-like computer operating systems that allows users to run programs with the security privileges of another user, by default the superuser.[6] It originally stood for "superuser do"[7] as the older versions of sudo were designed to run commands only as the superuser. However, the later versions added support for running commands not only as the superuser but also as other (restricted) users, and thus it is also commonly expanded as "substitute user do".[8][9] Although the latter case reflects its current functionality more accurately, sudo is still often called "superuser do" since it is so often used for administrative tasks.

Unlike the similar command su, users must, by default, supply their own password for authentication, rather than the password of the target user. After authentication, and if the configuration file, which is typically located at /etc/sudoers, permits the user access, the system invokes the requested command. The configuration file offers detailed access permissions, including enabling commands only from the invoking terminal; requiring a password per user or group; requiring re-entry of a password every time or never requiring a password at all for a particular command line. It can also be configured to permit passing arguments or multiple commands.


## Install

**ubuntu**

On ubuntu sudo installed by default.

**Debian**

```sh
su 
apt install -y sudo
```

## Configure passwordless call sudo

Edit file /etc/sudoers

```language
sudo vim /etc/sudoers
```

Add username(my name is human) to end of file. For get current user name type in terminal 

```sh
whoami
```

Output


```sh
human
```

Add username

```sh

human ALL=(ALL) NOPASSWD: ALL

```

Save file