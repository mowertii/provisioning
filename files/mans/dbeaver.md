# Dbeaver

## Description


Free multi-platform database tool for developers, database administrators, analysts and all people who need to work with databases. Supports all popular databases: MySQL, PostgreSQL, SQLite, Oracle, DB2, SQL Server, Sybase, MS Access, Teradata, Firebird, Apache Hive, Phoenix, Presto, etc.


## Install

**ubuntu**

```sh
export dir_libs_shell="/opt/scripts/shell"
if [[ ! -d "${dir_libs_shell}" ]];then sudo mkdir -p ${dir_libs_shell};fi
sudo chown -R $(whoami):$(whoami) ${dir_libs_shell}
curl -L https://gitlab.com/cdn72/provisioning/-/raw/master/files/libs.d/shell/common.sh -o ${dir_libs_shell}/common.sh
source ${dir_libs_shell}/common.sh
install_dbeaver
```